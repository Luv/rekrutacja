package tests;

import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedCondition;

public class AddElementTest {

	private WebDriver wd;
	private WebDriverWait wait;
	private Actions actions;
	private String driverPath;
	private String url;
	private String userEmail;
	private String userPassword;
	
	@Before
	public void setUp() {
		driverPath = "D:\\zadanie\\geckodriver-v0.20.0-win64\\geckodriver.exe";
		System.setProperty("webdriver.gecko.driver", driverPath);
		wd = new FirefoxDriver();
		wait = new WebDriverWait(wd,10);
		actions = new Actions(wd);
		url = "https://app.uxpin.com";
		userEmail = "myEmail@domain.com";
		userPassword = "myPassword";
	}
	
	/**
	 * Logs existing user in. Checks if after providing correct credentials 
	 * user is directed to projects dashboard and if user's data is correct.
	 * 
	 * @param userEmail		existing user's email
	 * @param userPass		correct user's password
	 */
	public void login(String userEmail, String userPass) {
		
		wd.get(url);
		
		WebElement loginEmail = wd.findElement(By.id("login-login"));
		loginEmail.clear();
		loginEmail.sendKeys(userEmail);
		
		WebElement loginPass = wd.findElement(By.id("login-password"));
		loginPass.clear();
		loginPass.sendKeys(userPass);
		
		wd.findElement(By.id("loginform_button1")).click();
		Assert.assertEquals(wd.getCurrentUrl(), "https://app.uxpin.com/dashboard/projects");
		
		wd.findElement(By.className("with-changelog")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[@class='account-name']")));
		Assert.assertTrue(wd.findElement(By.xpath("//p[@class='account-name']")).getText().equalsIgnoreCase(userEmail));
	}
	
	/**
	 * Creates new project for user who is already logged.
	 * Checks if user is directed to correct project page.
	 * 
	 * @param projectName	newly created project name
	 */
	public void createProject(String projectName) {
		
		wd.findElement(By.linkText("New project")).click();
		
		wd.findElement(By.id("project-name")).sendKeys(projectName);
		wd.findElement(By.xpath("//button[@class='btn-solid']")).click();
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[contains(text(),'Start from scratch')]")));
		Pattern pattern = Pattern.compile("https://app.uxpin.com/dashboard/project/\\d+");
		Matcher matcher = pattern.matcher(wd.getCurrentUrl());
		Assert.assertTrue(matcher.matches());
	
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[@class='name icon-general-chevron']")));
		Assert.assertTrue(wd.findElement(By.xpath("//h2[@class='name icon-general-chevron']")).getText().equals(projectName));
	}
	
	/**
	 * from https://www.testingexcellence.com/webdriver-wait-page-load-example-java/
	 */
    public void waitForLoad(WebDriver driver) {
        ExpectedCondition<Boolean> pageLoadCondition = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                    }
                };
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(pageLoadCondition);
    }
    
	/**
	 * Adds box element to the design
	 * Checks if box was correctly added.
	 */
	public void addBoxElementToTheDesign() {

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(),'Design With UXPin Editor')]")));
		WebElement designWithUXPinEditorButton = wd.findElement(By.xpath("//button[contains(text(),'Design With UXPin Editor')]"));
		actions.moveToElement(designWithUXPinEditorButton).click().perform();
	
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//ul[@class='toolbar-list quick-actions']//li[1]")));
		WebElement createBoxButton = wd.findElement(By.xpath("//ul[@class='toolbar-list quick-actions']//li[1]"));
		waitForLoad(wd);
		createBoxButton.click();
		
		WebElement box = wd.findElement(By.xpath("//div[@class='element-transformator ng-highlight-select transforming']"));
		Assert.assertTrue(box.getAttribute("style").equalsIgnoreCase("width: 300px; height: 100px; transform: rotate(0deg) translate(50px, 50px) rotate(0deg);"));
	}
	
	/**
	 * Opens preview of the design, checks if preview is opened and if Box element is displayed.
	 */
	public void seeTheDesignPreview() {

		String mainHandle = wd.getWindowHandle();
		wd.findElement(By.xpath("//a[@class='btn-icon ds-icon--font__arrow-play preview']")).click();
		wait.until(ExpectedConditions.numberOfWindowsToBe(2));
		Set<String> handles = wd.getWindowHandles();

		for (String handle : handles) {
			if (!handle.equals(mainHandle)) {
				wd.switchTo().window(handle);
				
				wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("ng-canvas-container")));
				
				Pattern p1 = Pattern.compile("https://preview.uxpin.com/.+");
				Matcher m1 = p1.matcher(wd.getCurrentUrl());
				Assert.assertTrue(m1.matches());  

				Assert.assertTrue(wd.findElement(By.xpath("//div[@class='ng-element el-box text-editor-light']")).isDisplayed());
			}
		}
	}
	
	/**
	 * Adds new or edits existing comment to design preview page.
	 * 
	 * @param newComment	comment to be added
	 * @param edited		true, if user edits existing comment
	 */
	public void commentTheDesign(String newComment, Boolean edited) {
		
		String oldComment;
		By commentPath = new By.ByXPath("/html[1]/body[1]/div[9]/main[1]/section[1]/section[1]/section[1]/section[1]/section[1]/section[2]/article[1]/div[1]/section[1]/div[1]/p[1]/span[1]/span[1]");
		
		if(edited.equals(false)) {
			oldComment = "";
			wd.findElement(By.linkText("Comment")).click();
			WebElement container = wd.findElement(By.className("ng-canvas-container"));
			wait.until(ExpectedConditions.elementToBeClickable(container));
			container.click();	
		} else{
			oldComment = wd.findElement(commentPath).getText();
			WebElement editIcon = wd.findElement(By.xpath("//a[@class='icon-general-cog only-icon-font']"));													
			actions.moveToElement(editIcon).perform();	
			wd.findElement(By.linkText("Edit comment")).click();
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//textarea[@placeholder='Edit your comment']")));
		}
		wd.findElement(By.className("fake-textarea")).sendKeys(newComment);
		wd.findElement(By.className("btn-solid")).click();			
		String fullComment = wd.findElement(commentPath).getText();
		Assert.assertTrue(fullComment.equals(oldComment + newComment));
	}
	
	@Test
	public void firstMethod() { 
		
		login(userEmail, userPassword);
		createProject("BoxProject");
		addBoxElementToTheDesign();
		seeTheDesignPreview();
		commentTheDesign("My comment", false);
		commentTheDesign(" is longer now.", true);
	}
	
	@After
	public void tearDown() {
		wd.quit();
	}
}
